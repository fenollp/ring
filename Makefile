all: erl.mk

erl.mk:
	wget -nv -O $@ 'https://raw.githubusercontent.com/fenollp/erl-mk/004b9b159bbca4225c573107f3c05c2b5558f05c/erl.mk' || rm $@

include erl.mk

# Your targets after this line.

distclean: clean clean-docs
	$(if $(wildcard deps/ ), rm -rf deps/)
	$(if $(wildcard logs/ ), rm -rf logs/)
	$(if $(wildcard erl.mk), rm erl.mk   )
.PHONY: distclean

test: eunit

dialyzer: all
	dialyzer -q -n ebin -Wunmatched_returns -Werror_handling -Wrace_conditions

debug: all
	erl -pa ebin/ -pa deps/*/ebin/
