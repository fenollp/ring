%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(ring).

%% ring: container implemented using a vector.

-export([new/0]).
-export([insert/2]).
-export([remove/2]).
-export([replace_nth/3]).
-export([nth/2]).
-export([peel_n_from/3]).
-export([has/2]).
-export([size/1]).
-export([from_list/1]).
-export([to_list/1]).

-type ring_type() :: tuple().
-export_type([ring_type/0]).

%% API

%% @doc Create an empty ring.
%% Complexity: constant.
-spec new() -> {}.
new() ->
    {}.

%% @doc Insert Item into Ring.
%% Complexity: O(n).
-spec insert(term(), ring_type()) -> ring_type().
insert(Item, Ring) ->
    erlang:append_element(Ring, Item).

%% @doc Create a new ring not containing Item.
%% Complexity: O(n).
-spec remove(term(), ring_type()) -> ring_type().
remove(Item, Ring) ->
    realloc_without(Item, Ring).

%% @doc Replace Nth element of Ring with Item. A modulo is used.
%% Complexity: constant.
-spec replace_nth(pos_integer(), term(), ring_type()) -> ring_type().
replace_nth(Nth, Item, Ring) ->
	Pos = case Nth rem ?MODULE:size(Ring) of
              0 -> ?MODULE:size(Ring);
              Index -> Index
          end,
    erlang:setelement(Pos, Ring, Item).

%% @doc Get Nth element of Ring. A modulo is used.
%% Complexity: constant.
-spec nth(pos_integer(), ring_type()) -> term().
nth(Nth, Ring) ->
    Pos = case Nth rem ?MODULE:size(Ring) of
              0 -> ?MODULE:size(Ring);
              Index -> Index
          end,
    erlang:element(Pos, Ring).

%% @doc Peel Ring from Pos to Len into a list.
%% Complexity: O(Len - Pos).
-spec peel_n_from(pos_integer(), pos_integer(), ring_type()) -> list().
peel_n_from(Len, Pos, Ring) when Pos > 0 ->
	[nth(I, Ring) || I <- lists:seq(Pos, Len + Pos - 1)].

%% @doc Tell whether Ring has Item.
%% Complexity: O(n) at most.
-spec has(term(), ring_type()) -> boolean().
has(Item, Ring) ->
    already_inside(Item, Ring).

%% @doc Size of Ring.
%% Complexity: constant.
-spec size(ring_type()) -> pos_integer().
size(Ring) ->
    erlang:tuple_size(Ring).

%% @doc Build a ring from List.
%% Complexity: O(n).
-spec from_list(list()) -> ring_type().
from_list(List) ->
    erlang:list_to_tuple(List).

%% @doc Build a list from Ring.
%% Complexity: O(n).
-spec to_list(ring_type()) -> [any()].
to_list(Ring) ->
    erlang:tuple_to_list(Ring).

%% Internals

already_inside(Item, Tuple) ->
    already_inside(Item, Tuple, erlang:tuple_size(Tuple)).
already_inside(_Item, _Tuple, 0) -> false;
already_inside(Item, Tuple, N) ->
    case erlang:element(N, Tuple) of
        Item -> true
        ; __ -> already_inside(Item, Tuple, N - 1)
    end.

realloc_without(Item, Tuple) ->
    erlang:list_to_tuple([X || I <- lists:seq(1, ?MODULE:size(Tuple)),
                               X <- [erlang:element(I, Tuple)],
                               X =/= Item]).

%% End of Module.
