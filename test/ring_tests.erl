%% Copyright © 2013 Pierre Fenoll
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(ring_tests).

%% ring_tests: tests for module ring.

-include_lib("eunit/include/eunit.hrl").


%% API tests.

new_test() ->
    {} = ring:new().

insert_test() ->
    {a, b} = ring:insert(b, {a}),
    {a, b, c} = ring:insert(c, {a, b}).

remove_test() ->
    {} = ring:remove(a, {}),
    {} = ring:remove(a, {a}),
    {a, c} = ring:remove(b, {a, b, c}).

replace_nth_test() ->
    Ring = {a, b, c},
    {a, b, c} = ring:replace_nth(1, a, Ring),
    {a, a, c} = ring:replace_nth(5, a, Ring),
    {a, b, a} = ring:replace_nth(9, a, Ring).

nth_test() ->
    Ring = {a, b, c},
    a = ring:nth(1, Ring),
    b = ring:nth(5, Ring),
    c = ring:nth(9, Ring).

peel_n_from_test() ->
    Ring = {a, b, c},
    [] = ring:peel_n_from(0, 1, Ring),
    [a] = ring:peel_n_from(1, 1, Ring),
    [a,b] = ring:peel_n_from(2, 1, Ring),
    [a,b,c] = ring:peel_n_from(3, 1, Ring),
    [b,c,a] = ring:peel_n_from(3, 2, Ring).

has_test() ->
    false = ring:has(a, {}),
    false = ring:has(a, {b}),
    true = ring:has(c, {a, b, c}).

size_test() ->
    0 = ring:size({}),
    1 = ring:size({a}),
    5 = ring:size({a,b,c,d,e}).

from_list_test() ->
    {} = ring:from_list([]),
    {a} = ring:from_list([a]),
    {a,b,c} = ring:from_list([a,b,c]).

to_list_test() ->
    [] = ring:to_list({}),
    [a] = ring:to_list({a}),
    [a,b,c] = ring:to_list({a,b,c}).

%% Internals

%% End of Module.
