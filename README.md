#ring • [Bitbucket](https://bitbucket.org/fenollp/ring)

## Overview

A ring container library application implemented using a tuple.

## Building

Just `make`.

## Add it as a dependency

Using `rebar`, add this line to your project's *rebar.config* `{deps, List}`:

    :::erlang
    {ring, ".*", {git, "https://bitbucket.org/fenollp/ring"}}
